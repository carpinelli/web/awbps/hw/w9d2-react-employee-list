#!/bin/bash

# .



function console()
{
    printf '%s\n' "$@"

    return 0
}

function gitSetup()
{
    # Removes project template's git folder
    rm -rf .git/
    # Setup Git for a new project
    git init --initial-branch=main
    groupPath="GET_GROUP_PATH_OR_WHOLE_URL_FROM_USER"
    repo_name=$(git rev-parse --show-toplevel | xargs basename).git
    branch_name=$(git rev-parse --abbrev-ref HEAD)
    console "Run: "
    console "$ git remote add origin git@gitlab.com:<namespace/username>/$groupPath/$repo_name $branch_name"
    # Self-hosted:
    #git remote add origin git@gitlab.example.com:<namespace/username>/$groupPath/$repo_name $branch_name
    git add .
    git commit -m "Initial commit."
    console "Run: "
    console "$ git push --set-upstream origin main"
    
    return 0
}


gitSetup $@
exit 0

