import { Routes, Route } from "react-router-dom";

import "./index.css";
import "./App.css";

import HomePage from "./pages/HomePage";
import EmployeePage from "./pages/EmployeePage";

const App = function()
{
  return (
    <main className="App">
      <Routes>
        <Route path="/" element={<HomePage />}/>
        <Route path="/:employeeParameter" element={<EmployeePage />} />
      </Routes>
    </main>
  );
};


export default App;
