import {useState} from "react"

import Form from "./UncontrolledForm"

const DisplayForm = function()
{
    const [isShown, setIsShown] = useState(false)
    const display = function()
    {
        setIsShown(current => !current)
    };
    
    return (
        <div>
            <button onClick={display}>
                {!isShown ? "Add Employee" : "Hide"}
            </button>
            {isShown && <Form />}
        </div>
    );
};


export default DisplayForm;
