const EmployeeCard = function({ employee })
{
  return (
    <li>
      <button className="employee-card" style={ {border: `1px solid ${employee.borderColor}`} }>
        <img src={employee.headshot} alt="" />
        <h3>{employee.name}</h3>
        <p>{employee.title}</p>
      </button>
    </li>
  );
};

// function Employee({ employee, removeEmployee }) {
//   const [showDelete, setShowDelete] = useState(false);

//   return (
//     <div
//       className="emp"
//       onMouseEnter={() => setShowDelete(true)}
//       onMouseLeave={() => setShowDelete(false)}
//       style={{ border: `1px solid ${employee.borderColor}` }}
//     >
//       <img src={employee.headshot} alt="employee" />
//       <span>
//         <h4>{employee.name}</h4>
//         {showDelete && (
//           // <h1 onClick={() => removeEmployee(employee.id)}>X</h1>
//           // <h1 onClick={removeEmployee(employee.id)}>X</h1>
//           <h1>X</h1>
//         )}{" "}
//       </span>
//       <p>{employee.title}</p>
//     </div>
//   );
// }


export default EmployeeCard;
