import InfoCard from "./InfoCard";

import React from "react";


const EmployeeDetails = function ({ employee })
{
    const phoneHeader1 = "Call Office";
    const phoneHeader2 = "Call Mobile";
    const phoneHeader3 = "SMS";
    const emailHeader = "Email";
    return (
        <form>
            <InfoCard header={phoneHeader1} info={employee.phone} />
            <InfoCard header={phoneHeader2} info={employee.phone} />
            <InfoCard header={phoneHeader3} info={employee.phone} />
            <InfoCard header={emailHeader} info={employee.email} />
        </form>
    );
};

export default EmployeeDetails;