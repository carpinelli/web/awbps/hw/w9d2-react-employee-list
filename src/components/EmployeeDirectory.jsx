import { Link } from "react-router-dom";

import { useEmployees } from "./EmployeesContext";
import EmployeeCard from "./EmployeeCard";


const EmployeeDirectory = function()
{
  const employees = useEmployees();

  return (
    <ul className="employee-list">
      {employees.map((employee) =>
      {
        return (
          <Link to={`/${employee.name}`} key={employee.id}>
            <EmployeeCard key={employee.id} employee={employee} />
          </Link>
        );
      })}
    </ul>
  );
};

export default EmployeeDirectory;
