import { createContext, useContext, useReducer } from "react";

import employees from "../data/employees";


const EmployeesContext = createContext(null);
const EmployeesDispatchContext = createContext(null);

// Custom useContext hook.
const useEmployees = function()
{
    return useContext(EmployeesContext);
};

const useEmployeesDispatch = function()
{
    return useContext(EmployeesDispatchContext);
};


// Reducer.

const ADD_EMPLOYEE = "ADD";
const REMOVE_EMPLOYEE = "DELETE";

const addEmployee = function(employee)
{
    return {
        type: ADD_EMPLOYEE,
        payload: employee,
    };
};

const removeEmployee = function(employeeID)
{
    return {
        type: REMOVE_EMPLOYEE,
        payload: employeeID,
    };
};

const employeesReducer = function(employees, action)
{
    switch (action.type)
    {
        case ADD_EMPLOYEE:
            const newEmployee = {
                name: action.payload.name,
                title: action.payload.title,
                phone: action.payload.phone,
                email: action.payload.email,
                headshot: `../images/headshot0.jpeg`,
                borderColor: "yellow",
                id: crypto.randomUUID(),
            };
           return [...employees, newEmployee];
        case REMOVE_EMPLOYEE:
            return employees.filter((employee) =>
            {
                return employee.id !== action.payload;
            });
    }
}


// Custom Provider component.
const EmployeeProvider = function({ children })
{
    const [employeeDirectory, dispatch] = useReducer(
        employeesReducer,
        employees
    );

    return (
        <EmployeesContext.Provider value={employeeDirectory}>
            <EmployeesDispatchContext.Provider value={dispatch}>
                {children}
            </EmployeesDispatchContext.Provider>
        </EmployeesContext.Provider>
    );
}


export default EmployeeProvider;
export
{
    useEmployees,
    useEmployeesDispatch,
    addEmployee,
    removeEmployee,
    employeesReducer,
    EmployeeProvider,
};
