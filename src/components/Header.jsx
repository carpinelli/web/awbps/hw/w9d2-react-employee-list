import { useNavigate } from "react-router-dom";

const Header = function ({ hasButton, header }) {
  const navigate = useNavigate();
  let button = null;

  if (hasButton)
  {
    button = (
      <button className="back-button" onClick={() => navigate("/")}>
        &lt;
      </button>
    );
  }

  return  (
      <div className="header">
        {button}
        <h2 className="header-text">{header}</h2>
      </div>
    );
};

export default Header;
