const InfoCard = function({ header, info })
{
  return (
    <div className="info-card">
      <div className="info-text">
        <h3>{header}</h3>
        <p>{info}</p>
      </div>
      <button className="expand-button"> &gt; </button>
    </div>
  );
};


export default InfoCard;

