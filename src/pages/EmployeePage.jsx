import { useParams } from "react-router-dom";

import Header from "../components/Header";
import EmployeeCard from "../components/EmployeeCard";
import EmployeeDetails from "../components/EmployeeDetails";

import { useEmployees } from "../components/EmployeesContext";

const EmployeePage = function()
{
  const employees = useEmployees();
  const { employeeParameter } = useParams();
  const HEADER = "Employee";
  const employee = employees.find((employee) =>
  {
    return employee.name === employeeParameter;
  });


  return (
    <section className="employee-page">
      <Header hasButton={true} header={HEADER} />
      <EmployeeCard
        className="large"
        employee={employee}
      />
      <EmployeeDetails employee={employee} />
    </section>
  );
};

export default EmployeePage;
