import Header from "../components/Header";
import SearchBar from "../components/SearchBar";
import EmployeeDirectory from "../components/EmployeeDirectory";
import DisplayForm from "../components/DisplayForm";

const HomePage = function() {
  const HEADER = "Employee Directory";
  return (
    <article className="home-page">
      <Header hasButton={false} header={HEADER} />
      <SearchBar />
      <EmployeeDirectory />
      <DisplayForm />
    </article>
  );
};

export default HomePage;
